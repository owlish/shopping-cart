import java.util.ArrayList;

public class Cart {
    private ArrayList<CartItem> cartItems;
    private PriceCalculator pc;
    public Cart() {
        cartItems = new ArrayList<CartItem>();
        pc = new PriceCalculator();
    }

    public double getotalPrice() throws Exception {
        return pc.calcTotalPrice(cartItems);
    }

    public void addItem(Product p, int quantity) {
        cartItems.add(new CartItem(p.getId(), quantity, 1));
    }

    public ArrayList<CartItem> getCartItems() {
        return cartItems;
    }
}
