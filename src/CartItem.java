public class CartItem {
    private int productId;
    private int quantity;
    private double discount;

    public CartItem(int productId, int quantity, double discount) {
        this.productId = productId;
        this.quantity = quantity;
        this.discount = discount;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getDiscount() {
        return discount;
    }

    public int getProductId() {
        return productId;
    }
}
