import java.util.ArrayList;

public class Market {
    private static Market ourInstance = new Market();
    private ArrayList<Product> products;

    public static Market getInstance() {
        return ourInstance;
    }

    private Market() {
        products = new ArrayList<Product>();
    }

    public double getProductPrice(int productId) throws Exception {
        for (Product p: products) {
            if (p.getId() == productId) {
                return p.getPrice();
            }
        }
        throw new Exception("pid mismatch");
    }

    public Product getProductById(int productId) throws Exception {
        for (Product p: products) {
            if (p.getId() == productId) {
                return p;
            }
        }
        throw new Exception("pid mismatch");
    }

    public void addProduct(Product p) {
        products.add(p);
    }

    public ArrayList<Product> getProducts() {
        return products;
    }
}
