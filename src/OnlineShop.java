import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

public class OnlineShop {
    private static Market market;
    private static Cart cart;
    private static Connection connection;

    public static void main(String[] args) throws Exception {
        market = Market.getInstance();
        cart = new Cart();
        connection = createDBConnection();
        try {
            setInitialProducts();
            getProducts();
            //getProductsNoDB();
        } catch (Exception e) {
            e.printStackTrace();
        }

        int order = -1;
        showMenu();
        while (order != 0) {
            try {
                order = updateCart();
            }  catch (Exception e) {
                e.printStackTrace();
            }
        }
        showCart();

        closeDBConnection(connection);
    }

    private static void showMenu() {
        ArrayList<Product> products = market.getProducts();
        System.out.println("Hi! :) Welcome to our shop! \nPlease choose your product.");
        for (Product p: products) {
            System.out.println(p.getId() + "." + p.getName() + "\t\t\t\t" + p.getPrice() + "$");
        }
        System.out.println("Enter 0 to finalize your shopping cart.");
    }

    private static int updateCart() throws Exception {
        Scanner reader = new Scanner(System.in);
        int order = reader.nextInt();
        if (order==0) {
            return order;
        }

        System.out.println("How many of them you need?");
        int quantity = reader.nextInt();
        cart.addItem(market.getProductById(order), quantity);
        System.out.println("Is there any other item? (Enter 0 to finish.)");
        return order;
    }

    private static void showCart() throws Exception {
        ArrayList<CartItem> cartItems = cart.getCartItems();
        for (CartItem ci: cartItems) {
            Product p = market.getProductById(ci.getProductId());
            System.out.println(p.getName() + "\t\t\t\t" + ci.getQuantity() + "*" + p.getPrice() + "$");
        }
        System.out.println("Total Price: " + cart.getotalPrice() + "$\n");
    }

    private static void getProductsNoDB() {
        market.addProduct(new Product(1, "a", 100));
        market.addProduct(new Product(2, "b", 200));
        market.addProduct(new Product(3, "c", 300));
    }

    private static void getProducts() throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery("SELECT * FROM products");
        while(rs.next())
        {
            // read the result set
            Product newProduct = new Product(rs.getInt("id"), rs.getString("name"), rs.getDouble("price") );
            market.addProduct(newProduct);
        }
    }

    private static Connection createDBConnection() throws Exception {
        try
        {
            // create a database connection
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:shopdb.sqlite");

            return connection;
        }
        catch(SQLException e)
        {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.err.println(e.getMessage());
        }
        return null;
    }

    private static void closeDBConnection(Connection connection) {
        try
        {
            if(connection != null)
                connection.close();
        }
        catch(SQLException e)
        {
            // connection close failed.
            System.err.println(e);
        }
    }

    public static void setInitialProducts() throws Exception {
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(30);  // set timeout to 30 sec.

        statement.executeUpdate("DROP TABLE IF EXISTS products");
        statement.executeUpdate("CREATE TABLE products(" +
                "id INT PRIMARY KEY DEFAULT 0," +
                "name VARCHAR(200) NOT NULL," +
                "price DOUBLE NOT NULL)");

        statement.executeUpdate("INSERT INTO products VALUES(1, 'Monitor', 100)");
        statement.executeUpdate("INSERT INTO products VALUES(2, 'Laptop', 200)");
        statement.executeUpdate("INSERT INTO products VALUES(3, 'TV', 300)");
    }
}
