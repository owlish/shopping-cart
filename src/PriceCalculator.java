import java.util.ArrayList;

public class PriceCalculator {
    private Market market = Market.getInstance();

    public double calcTotalPrice(ArrayList<CartItem> cartItems) throws Exception {
        double totalPrice = 0;
        double productPrice;
        // PRICE CALCULATION METHOD
        for (CartItem ci: cartItems) {
            productPrice = market.getProductPrice(ci.getProductId());
            totalPrice += productPrice * ci.getDiscount() * ci.getQuantity();
        }
        return totalPrice;
    }
}
